import numpy as np
import xml.etree.ElementTree as ET
from collections import defaultdict
import cv2 as cv

mat = np.fromfile('00006006.img', "uint8") # import raw image
# ----- shape of the raw file
# print(mat.shape)
mat = np.reshape(mat, (2048, 2048)).transpose() # reshape it to 2048*2048
# cv.imshow("img",mat)
# cv.waitKey()
# cv.destroyAllWindows()
# print(mat[580:590, 580:590]),
# params = list()
# params.append(cv.CV_IMWRITE_PNG_COMPRESSION)
# params.append(0)

cv.imwrite("a.png", mat) # write proper image to "a.jpg"

root = ET.parse('calibration_data.xml').getroot() # XML file iterator
# print(root)
# mat1, mat2 = np.empty([2048, 2048]), np.empty([2048, 2048])

# fetching two coefficients for undistortion
camera_metrix = np.reshape(np.fromstring(root[9][3].text, dtype=np.double, sep=' '), (3, 3))
distortion_coefficients = np.fromstring(root[10][3].text, dtype=np.double, sep=' ')

# print(camera_metrix, distortion_coefficients)
newCameraMatrix, poi = cv.getOptimalNewCameraMatrix(camera_metrix,
                                                    distortion_coefficients,
                                                    (2048, 2048), 1, (2048, 2048))

map1, map2 = cv.initUndistortRectifyMap(camera_metrix,
                                        distortion_coefficients,
                                        None,
                                        newCameraMatrix,
                                        (2048, 2048),
                                        cv.CV_16SC2)

mat = cv.remap(mat, map1, map2, cv.INTER_LINEAR)

z = 7
offset = z * 50
temp1 = cv.resize(mat, (2048*z, 2048*z))
# temp2 = temp1(cv.Rect((temp1.rows*(0.5 - 1/z) - offset, temp1.cols*(0.5 - 1/z) - offset),(temp1.rows*(0.5 + 1/z) - offset, temp1.cols*(0.5 + 1/z) - offset)))
temp2 = temp1[int(temp1.shape[0]*(0.5 - 1/z) - offset ): int(temp1.shape[0]*(0.5 + 1/z) - offset), int(temp1.shape[1]*(0.5 - 1/z) - offset) : int(temp1.shape[1]*(0.5 + 1/z) - offset)]

mat = cv.resize(temp2, (int(temp1.shape[0]*0.5),int(temp1.shape[0]*0.5)))


cv.imwrite("b.jpg", mat)
print(mat.shape)
#cv.imshow('img', dst)
#cv.waitKey()
#cv.destroyAllWindows()