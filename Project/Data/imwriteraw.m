function imwriteraw(image, filename)
%IMWRITERAW Write 8-bit grey level camera image file.
%  Usage: IMREADRAW(IMAGE, FILENAME)
%
%  Parameters: filename - image filename
%              image    - matrix containing image 

%  Author: Steve Gunn (srg@ecs.soton.ac.uk)

fid = fopen(filename, 'w');
fwrite(fid, image, 'uint8');
fclose(fid);