function im = implot(filename)
%IMREADRAW Read 8-bit grey level camera image file.
%  Usage: IMAGE = IMREADRAW(FILENAME)
%
%  Parameters: filename - image filename
%              image    - matrix containing image 

%  Author: Steve Gunn (srg@ecs.soton.ac.uk)

fid = fopen(filename, 'r');
im = fread(fid, [2048, 2048], 'uint8=>uint8');
fclose(fid);
image(im);
