load 'histogram.ris'
[y, i] = sort(histogram(:,1));
chist = histogram(i,:);
for j=256:-1:2
    chist(:,j) = chist(:,j) + chist(:,j+1);
end
save 'chistogram.mat' chist
