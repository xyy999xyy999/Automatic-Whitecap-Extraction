import gluonbook as gb
from mxnet import autograd as ag
from mxnet import nd
from mxnet import gluon, init
import numpy as np
import mxnet as mx
from mxnet.gluon import data as gdata, nn, loss as gloss

# import os
# print(os.getcwd())

PATH = 'processed/'
cnt = 0
percent = 0.2
size = 256
NUM = 1000
batch_size = 100

gpus = mx.test_utils.list_gpus()
ctx =  mx.gpu() if gpus else mx.cpu()

t_data = nd.load("checkpoint.params")
train_matrix= t_data['train_matrix']
test_matrix = t_data['test_matrix']
y_train = t_data['y_train']
y_test = t_data['y_test']


#print(t_data.train_matrix.shape, t_data.train_matrix[0].shape, t_data.train_matrix[0][1].dtype)

transformer = gdata.vision.transforms.ToTensor()
train_iter = gdata.DataLoader((transformer(train_matrix), y_train), batch_size, shuffle=True, num_workers=4)
test_iter = gdata.DataLoader((transformer(test_matrix), y_test), batch_size, shuffle=False, num_workers=4)


net = nn.Sequential()
net.add(nn.Flatten())
net.add(nn.Dense(2))
net.initialize(init.Normal(sigma=0.01))

loss = gloss.SoftmaxCrossEntropyLoss()

trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': 0.02})

num_epochs = 5

def accuracy(y_hat, y):
    return (y_hat.argmax(axis=1) == y.astype('float32')).mean().asscalar()


def evaluate_accuracy(data_iter, net):
    acc = 0
    for X, y in data_iter:
        acc += accuracy(net(X), y)
    return acc / len(data_iter)

def train(net, train_iter, test_iter, loss, num_epochs, batch_size, params=None, lr=None, trainer=None):
    for epoch in range(1, num_epochs + 1):
        train_l_sum = 0
        train_acc_sum = 0
        for X in train_iter:
            print("done")
            with ag.record():
                y_hat = net(X)
                l = loss(y_hat, y_train)
                print("done")
            l.backward()
            print("done")
            if trainer is None:
                gb.sgd(params, lr, batch_size)
            else:
                trainer.step(batch_size)
            train_l_sum += l.mean().asscalar()
            train_acc_sum += accuracy(y_hat, y)
        test_acc = evaluate_accuracy(test_iter, net)
        print('epoch %d, loss %.4f, train acc %.3f, test acc %.3f'
              % (epoch, train_l_sum / len(train_iter),
                 train_acc_sum / len(train_iter), test_acc))

train(net, train_iter, test_iter, loss, num_epochs, batch_size, None, None, trainer)