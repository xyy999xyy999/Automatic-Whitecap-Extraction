import numpy as np
import pandas as pd
import cv2 as cv
from sklearn.model_selection import train_test_split

PATH = 'processed/'
cnt = 0
labels = pd.read_excel('label_new.xlsx', columns = ['image', 'label'])
X = np.array(labels['image'])
y = np.array(labels['label'])
X_train_name, X_test_name, y_train, y_test = train_test_split(X, y, test_size = 0.2)

#print(X_train_name, y_train)

train_matrix = np.zeros((800, 2048, 2048), dtype = np.uint8)
test_matrix = np.zeros((200, 2048, 2048), dtype = np.uint8)
for fn in X_train_name:
    x = cv.imread(PATH + fn)
    x = cv.cvtColor(x, cv.COLOR_BGR2GRAY)
    train_matrix[cnt] = x
    #print(cnt)
    cnt += 1

cnt = 0
for fn in X_test_name:
    x = cv.imread(PATH + fn)
    x = cv.cvtColor(x, cv.COLOR_BGR2GRAY)
    test_matrix[cnt] = x
    #print(cnt)
    cnt += 1

np.save("train_name", X_train_name)
np.save("test_name", X_test_name)
np.save("train_matrix", train_matrix)
np.save("test_matrix", test_matrix)
np.save("train_label", y_train)
np.save("test_label", y_test)
print("Done saving")