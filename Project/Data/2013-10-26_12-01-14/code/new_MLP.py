from __future__ import print_function
import mxnet as mx
import mxnet.ndarray as nd
import mxnet.gluon as gluon
from mxnet.gluon import nn
from mxnet import autograd as ag
import numpy as np
import pandas as pd
import cv2 as cv
from sklearn.model_selection import train_test_split

# import os
# print(os.getcwd())

PATH = 'processed/'
cnt = 0
percent = 0.2
size = 256
NUM = 1000
batch_size = 100

gpus = mx.test_utils.list_gpus()
ctx =  [mx.gpu()] if gpus else mx.cpu()

t_data = nd.load("histograms.params")
train_matrix= t_data['train_matrix']
test_matrix = t_data['test_matrix']
y_train = t_data['y_train']
y_test = t_data['y_test']

train_data = mx.io.NDArrayIter(train_matrix, y_train, batch_size, shuffle=True)
test_data = mx.io.NDArrayIter(test_matrix, y_test, batch_size)

net = nn.Sequential()
with net.name_scope():
    net.add(nn.Dense(256, activation='tanh'))
    net.add(nn.Dense(64, activation='tanh'))
    net.add(nn.Dense(2))

net.initialize(mx.init.Xavier(magnitude=2.24), ctx=ctx)
trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': 0.02 })

epoch = 10
metric = mx.metric.Accuracy()
softmax_cross_entropy_loss = gluon.loss.SoftmaxCrossEntropyLoss()
for i in range(epoch):
    train_data.reset()

    for batch in train_data:
        data = gluon.utils.split_and_load(batch.data[0], ctx_list=[ctx], batch_axis=0)
        label = gluon.utils.split_and_load(batch.label[0], ctx_list=[ctx], batch_axis=0)
        outputs = []

        with ag.record():
            for x, y in zip(data, label):
                z = net(x)
                loss = softmax_cross_entropy_loss(z, y)
                loss.backward()
                outputs.append(z)

        metric.update(label, outputs)
        trainer.step(batch.data[0].shape[0])
    name, acc = metric.get()
    metric.reset()
    print('training acc at epoch %d: %s = %f'%(i, name, acc))

metric = mx.metric.Accuracy()
test_data.reset()

for batch in test_data:
    data = gluon.utils.split_and_load(batch.data[0], ctx_list=[ctx])
    label = gluon.utils.split_and_load(batch.label[0], ctx_list=[ctx])
    outputs = []
    for x in data:
        # print(x, x.shape, x.dtype, x.stype)
        #print(net(x))
        outputs.append(net(x))

    metric.update(label, outputs)
out = nd.zeros(100)
cnt = 0
for i in outputs[0]:
    out[cnt] = (nd.max(i))
    cnt += 1
out = out.astype('int32')
label = label[0].astype('int32')
#print(out.shape)
diff = []
for i in range(len(label)):
    print(label[i], out[i])
    if nd.abs(label[i] - out[i]) == 0:
        diff.append(i)
print(diff)#, diff)
print('validation acc: %s=%f' % metric.get())

net.collect_params().save('MLP.params')
