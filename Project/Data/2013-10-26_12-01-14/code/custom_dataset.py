import collections
import mxnet as mx
from mxnet.gluon.data import dataset
import os
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

mx.random.seed(42)

plt.interactive(False)

class ImageWithMaskDataset(dataset.Dataset):
    """
    A dataset for loading images (with masks) stored as `xyz.jpg` and `xyz_mask.png`.

    Parameters
    ----------
    root : str
        Path to root directory.
    transform : callable, default None
        A function that takes data and label and transforms them:
    ::
        transform = lambda data, label: (data.astype(np.float32)/255, label)
    """
    def __init__(self, root, transform=None):
        self._root = os.path.expanduser(root)
        self._transform = transform
        self._exts = ['.bmp', '.png']
        self._list_images(self._root)

    def _list_images(self, root):
        images = collections.defaultdict(dict)
        for filename in sorted(os.listdir(root)):
            name, ext = os.path.splitext(filename)
            mask_flag = name.endswith("_mask")
            if ext.lower() not in self._exts:
                continue
            if not mask_flag:
                images[name]["base"] = filename
            else:
                name = name[:-5] # to remove '_mask'
                images[name]["mask"] = filename
        self._image_list = list(images.values())

    def __getitem__(self, idx):
        assert 'base' in self._image_list[idx], "Couldn't find base image for: " + image_list[idx]["mask"]
        base_filepath = os.path.join(self._root, self._image_list[idx]["base"])

        with open(base_filepath, 'rb') as fp:
            str_base = fp.read()
        base = mx.img.imdecode(str_base, flag=0)
        #base = mx.image.imread(base_filepath)
        assert 'mask' in self._image_list[idx], "Couldn't find mask image for: " + image_list[idx]["base"]
        mask_filepath = os.path.join(self._root, self._image_list[idx]["mask"])
        # print(mask_filepath)
        with open(mask_filepath, 'rb') as fp:
            str_mask = fp.read()
        mask = mx.img.imdecode(str_mask, flag=0)
        # print(mask.shape)
        #mask = mx.image.imread(mask_filepath)
        if self._transform is not None:
            return self._transform(base, mask)
        else:
            return base, mask

    def __len__(self):
        return len(self._image_list)

image_dir = os.path.join("data", "testing")
dataset = ImageWithMaskDataset(root=image_dir)

# sample = dataset.__getitem__(10)
# sample_base = sample[0].astype('uint8')
# sample_mask = sample[1].astype('uint8')



