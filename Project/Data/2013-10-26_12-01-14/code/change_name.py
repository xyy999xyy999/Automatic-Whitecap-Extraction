from glob import glob
import os
import re

PATH = "./segmentation"
os.chdir(PATH)
files = glob(r"./*.bmp")

for filename in files:
    name = filename.split('.')
    new_name = re.findall(r'\d+', name[1])[0]
    print(int(str(new_name)))
    os.rename(filename, str(new_name) + "_mask.bmp")