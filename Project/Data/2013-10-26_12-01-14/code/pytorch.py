import sys
import os
import os.path
import random
import collections
import shutil
import time
import glob
import csv
import numpy as np

import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data as data
import torchvision.datasets as datasets
import torchvision.models as models
import torchvision.transforms as transforms


train_matrix = np.load('train_matrix.npy')
test_matrix = np.load('test_matrix.npy')
train_label = np.load('train_label.npy')
test_label = np.load('test_label.npy')
#print(train_metrics.shape, test_metrics.shape, train_label.shape, test_label.shape)

# data
batch_size = 8
n_train_sample = train_matrix.shape[0]
n_test_sample = test_matrix.shape[0]

# model
n_runs = 1
# n_aug = 3
epochs = 35
lr = 1e-4
clip = 0.001
archs = ["resnet152"]

model_names = sorted(name for name in models.__dict__ if name.islower() and not name.startswith("__"))
best_prec1 = 0
print(model_names)

class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def train(train_loader, model, criterion, optimizer, epoch):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    acc = AverageMeter()
    end = time.time()

    model.train()

    for i, (images, target) in enumerate(train_loader):
        data_time.update(time.time() - end)
        target = target.cuda(async=True)
        image_var = torch.autograd.Variable(images)
        label_var = torch.autograd.Variable(target)

        y_pred = model(image_var)
        loss = criterion(y_pred, label_var)

        prec1, prec = accuracy(y_pred.data, target, topk=(1, 1))
        losses.update(loss.data[0], images.size(0))
        acc.update(prec1[0], images.size(0))

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        batch_time.update(time.time() - end)
        end = time.time()


def test(test_loader, model):
    csv_map = collections.defaultdict(float)
    model.eval()

    for i, (images, name) in enumerate(test_loader):
        image_var = torch.autograd.Variable(images)
        y_pred = model(image_var)

        smax = nn.Softmax()
        smax_out = smax(y_pred)[0]
        under_prob = smax_out.data[0]
        above_prob = smax_out.data[1]
        prob = above_prob
        if under_prob > above_prob:
            prob = 1 - under_prob
        prob = np.round(prob, decimals=4)
        prob = np.clip(prob, clip, 1-clip)
        csv_map[name] +=prob

    sub_fn = '{0}epoch_{1}clip_{2}runs'.format(epochs, clip, n_runs)

    for arch in archs:
        sub_fn += "_{}".format(arch)

    print("Writing Predictions to CSV...")
    with open(sub_fn + '.csv', 'w') as csvfile:
        csv_w = csv.writer(csvfile)
        csv_w.writerow(('name', 'label'))
        for row in sorted(csv_map.items()):
            csv_w.writerow(row)
    print("Done.")

def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'model_best.pth.tar')

def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    global lr
    lr = lr * (0.1**(epoch // 30))
    for param_group in optimizer.state_dict()['param_groups']:
        param_group['lr'] = lr


def accuracy(y_pred, y_actual, topk=(1, )):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = y_actual.size(0)

    _, pred = y_pred.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(y_actual.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))

    return res