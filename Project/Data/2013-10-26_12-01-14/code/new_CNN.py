from __future__ import print_function
import mxnet as mx
import mxnet.gluon as gluon
from mxnet.gluon import nn
from mxnet import autograd as ag
import mxnet.ndarray as F
import numpy as np
import pandas as pd
import cv2 as cv
from sklearn.model_selection import train_test_split

# import os
# print(os.getcwd())

PATH = 'processed/'
cnt = 0
percent = 0.2
size = 256
NUM = 1000
batch_size = 100

gpus = mx.test_utils.list_gpus()
ctx =  mx.gpu() if gpus else mx.cpu()

t_data = F.load("histograms.params")
train_matrix= t_data['train_matrix']
test_matrix = t_data['test_matrix']
y_train = t_data['y_train']
y_test = t_data['y_test']

train_matrix = mx.nd.reshape(train_matrix, (train_matrix.shape[0], 1, 16, 16))
test_matrix = mx.nd.reshape(test_matrix, (test_matrix.shape[0], 1, 16, 16))

print(train_matrix.shape, test_matrix.shape, train_matrix.dtype)

train_data = mx.io.NDArrayIter(train_matrix, y_train, batch_size, shuffle=True)
test_data = mx.io.NDArrayIter(test_matrix, y_test, batch_size)

class Net(gluon.Block):
    def __init__(self, **kwargs):
        super(Net, self).__init__(**kwargs)
        with self.name_scope():
            # layers created in name_scope will inherit name space
            # from parent layer.
            self.conv1 = nn.Conv2D(20, kernel_size=(5,5))
            self.pool1 = nn.MaxPool2D(pool_size=(2,2), strides = (2,2))
            self.conv2 = nn.Conv2D(50, kernel_size=(5,5))
            self.pool2 = nn.MaxPool2D(pool_size=(2,2), strides = (2,2))
            self.fc1 = nn.Dense(500)
            self.fc2 = nn.Dense(10)

    def forward(self, x):
        x = self.pool1(F.tanh(self.conv1(x)))
        x = self.pool2(F.tanh(self.conv2(x)))
        # 0 means copy over size from corresponding dimension.
        # -1 means infer size from the rest of dimensions.
        x = x.reshape((0, -1))
        x = F.tanh(self.fc1(x))
        x = F.tanh(self.fc2(x))
        return x

net = Net()

net.initialize(mx.init.Xavier(magnitude=2.24), ctx=ctx)
trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': 0.02})

epoch = 5
metric = mx.metric.Accuracy()
softmax_cross_entropy_loss = gluon.loss.SoftmaxCrossEntropyLoss()

for i in range(epoch):
    # Reset the train data iterator.
    train_data.reset()
    # Loop over the train data iterator.
    for batch in train_data:
        # Splits train data into multiple slices along batch_axis
        # and copy each slice into a context.
        data = gluon.utils.split_and_load(batch.data[0], ctx_list=[ctx], batch_axis=0)
        # print(data, len(data))
        # Splits train labels into multiple slices along batch_axis
        # and copy each slice into a context.
        label = gluon.utils.split_and_load(batch.label[0], ctx_list=[ctx], batch_axis=0)
        outputs = []
        # Inside training scope
        with ag.record():
            for x, y in zip(data, label):
                z = net(x)
                # Computes softmax cross entropy loss.
                loss = softmax_cross_entropy_loss(z, y)
                # Backpropogate the error for one iteration.
                loss.backward()
                outputs.append(z)
        # Updates internal evaluation
        metric.update(label, outputs)
        # Make one step of parameter update. Trainer needs to know the
        # batch size of data to normalize the gradient by 1/batch_size.
        trainer.step(batch.data[0].shape[0])
    # Gets the evaluation result.
    name, acc = metric.get()
    # Reset evaluation result to initial state.
    metric.reset()
    print('training acc at epoch %d: %s=%f'%(i, name, acc))

metric = mx.metric.Accuracy()
test_data.reset()

for batch in test_data:
    data = gluon.utils.split_and_load(batch.data[0], ctx_list=[ctx])
    label = gluon.utils.split_and_load(batch.label[0], ctx_list=[ctx])
    outputs = []
    for x in data:
        outputs.append(net(x))
    metric.update(label, outputs)
print('test acc: %s = %f' % metric.get())
net.collect_params().save('CNN.params')
