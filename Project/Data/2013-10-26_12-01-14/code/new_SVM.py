import mxnet.ndarray as nd
from sklearn import svm, metrics

t_data = nd.load("histograms.params")
train_matrix= t_data['train_matrix'].asnumpy()
test_matrix = t_data['test_matrix'].asnumpy()
y_train = t_data['y_train'].asnumpy()
y_test = t_data['y_test'].asnumpy()

train_samples = len(train_matrix)
test_sample = len(test_matrix)
data_train = train_matrix.reshape((train_samples, -1))
data_test = test_matrix.reshape((test_sample, -1))

print(data_train, data_train.shape)
classifier = svm.SVC(gamma=0.001)
classifier.fit(data_train, y_train)

y_pred = classifier.predict(data_test)
print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(y_test, y_pred)))

print("Confusion matrix:\n%s" % metrics.confusion_matrix(y_test, y_pred))