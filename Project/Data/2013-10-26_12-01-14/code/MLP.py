from __future__ import print_function
import mxnet as mx
import mxnet.gluon as gluon
from mxnet.gluon import nn
from mxnet import autograd as ag
import numpy as np
import pandas as pd
import cv2 as cv
from sklearn.model_selection import train_test_split

# import os
# print(os.getcwd())

PATH = 'processed/'
cnt = 0
percent = 0.2
size = 256
NUM = 1000
batch_size = 100

gpus = mx.test_utils.list_gpus()
ctx =  [mx.gpu()] if gpus else mx.cpu()

labels = pd.read_excel('./data/label_new.xlsx', columns = ['image', 'label'])
X = np.array(labels['image'])
y = np.array(labels['label'])

X_train_name, X_test_name, y_train, y_test = train_test_split(X, y, test_size = percent)

def data_storage(name, size, percent, cnt=0):
    matrix = mx.nd.zeros((int(NUM * percent), size, size), ctx=ctx, dtype='uint8')
    for fn in name:
        x = cv.imread(PATH + fn)
        x = cv.cvtColor(x, cv.COLOR_BGR2GRAY)
        x = cv.resize(x, (size, size))
        matrix[cnt] = x
        cnt += 1
    matrix = matrix.astype('float32')
    return matrix

train_matrix = data_storage(X_train_name, size, 1 - percent, cnt=0)
test_matrix = data_storage(X_test_name, size, percent, cnt=0)

print(train_matrix.shape, test_matrix.shape, train_matrix.dtype)

train_data = mx.io.NDArrayIter(train_matrix, y_train, batch_size, shuffle=True)
test_data = mx.io.NDArrayIter(test_matrix, y_test, batch_size)

net = nn.Sequential()
with net.name_scope():
    net.add(nn.Dense(128, activation='relu'))
    net.add(nn.Dense(64, activation='relu'))
    net.add(nn.Dense(2))

net.initialize(mx.init.Xavier(magnitude=2.24), ctx=ctx)
trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': 0.02 })

epoch = 10
metric = mx.metric.Accuracy()
softmax_cross_entropy_loss = gluon.loss.SoftmaxCrossEntropyLoss()
for i in range(epoch):
    train_data.reset()

    for batch in train_data:
        data = gluon.utils.split_and_load(batch.data[0], ctx_list=[ctx], batch_axis=0)
        label = gluon.utils.split_and_load(batch.label[0], ctx_list=[ctx], batch_axis=0)
        outputs = []

        with ag.record():
            for x, y in zip(data, label):
                z = net(x)
                loss = softmax_cross_entropy_loss(z, y)
                loss.backward()
                outputs.append(z)

        metric.update(label, outputs)
        trainer.step(batch.data[0].shape[0])
    name, acc = metric.get()
    metric.reset()
    print('training acc at epoch %d: %s = %f'%(i, name, acc))

metric = mx.metric.Accuracy()
test_data.reset()

for batch in test_data:
    data = gluon.utils.split_and_load(batch.data[0], ctx_list=[ctx])
    label = gluon.utils.split_and_load(batch.label[0], ctx_list=[ctx])
    outputs = []
    for x in data:
        print(x, x.shape, x.dtype, x.stype)
        #print(net(x))
        outputs.append(net(x))

    metric.update(label, outputs)
print('validation acc: %s=%f' % metric.get())

net.collect_params().save('MLP.params')
