import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score

X_train = np.load('train_matrix.npy')
X_test = np.load('test_matrix.npy')
y_train = np.load('train_label.npy')
y_test = np.load('test_label.npy')

X_train = np.reshape(X_train, (800, 4194304))
X_test = np.reshape(X_test, (200, 4194304))
print("reshape done")
# Initialize our classifier
gnb = GaussianNB()
print("gnb done")

# Train our classifier
model = gnb.fit(X_train, y_train)
print("fit done")

# Make predictions
preds = gnb.predict(X_test)
print(preds)
print("pred done")

# Evaluate accuracy
print(accuracy_score(y_test, preds))
print("reshape done")
