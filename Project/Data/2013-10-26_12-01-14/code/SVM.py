import numpy as np

from sklearn import svm, metrics

train_matrix = np.load('train_matrix.npy')
test_matrix = np.load('test_matrix.npy')
train_label = np.load('train_label.npy')
test_label = np.load('test_label.npy')
#print(train_metrics.shape, test_metrics.shape, train_label.shape, test_label.shape)

train_samples = len(train_matrix)
test_sample = len(test_matrix)
data_train = train_matrix.reshape((train_samples, -1))
data_test = test_matrix.reshape((test_sample, -1))

classifier = svm.SVC(gamma = 0.001)
classifier.fit(data_train, train_label)

y_pred = classifier.predict(data_test)
print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(test_label, y_pred)))

print("Confusion matrix:\n%s" % metrics.confusion_matrix(test_label, y_pred))