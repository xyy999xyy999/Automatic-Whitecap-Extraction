import mxnet as mx
import mxnet.ndarray as nd
import pandas as pd
import numpy as np
import cv2 as cv
from sklearn.model_selection import train_test_split

PATH = 'processed/'
cnt = 0
percent = 0.2
NUM = 1000
batch_size = 100

gpus = mx.test_utils.list_gpus()
ctx  = mx.gpu() if gpus else mx.cpu()

labels = pd.read_excel('./data/label_new.xlsx', columns = ['image', 'label'])
X = np.array(labels['image'])
y = np.array(labels['label'])

X_train_name, X_test_name, y_train, y_test = train_test_split(X, y, test_size = percent)

y_train = mx.nd.array(y_train)
y_test = mx.nd.array(y_test)
def hist_storage(name, percent, cnt=0):
    matrix = mx.nd.zeros((int(NUM * percent), 256, 1), ctx=ctx, dtype='float32')
    for fn in name:
        x = cv.imread(PATH + fn)
        x = cv.cvtColor(x, cv.COLOR_BGR2GRAY)
        x = nd.array(cv.calcHist([x], [0], None, [256], [0, 256]))
        matrix[cnt] = x
        cnt += 1
        # break
    return matrix

train_matrix = hist_storage(X_train_name, 1 - percent, cnt=0)
test_matrix = hist_storage(X_test_name, percent, cnt=0)

print(train_matrix[799], train_matrix.shape, train_matrix.dtype)

t_data = {"train_matrix": train_matrix, "y_train": y_train, "test_matrix": test_matrix, "y_test": y_test}
filename = "histograms.params"
nd.save(filename, t_data)