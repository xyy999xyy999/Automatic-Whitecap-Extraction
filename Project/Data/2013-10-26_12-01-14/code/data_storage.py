from mxnet import nd
import pandas as pd
import numpy as np
import cv2 as cv
from sklearn.model_selection import train_test_split
import mxnet as mx

PATH = 'processed/'
cnt = 0
percent = 0.2
size = 256
NUM = 1000
batch_size = 100

gpus = mx.test_utils.list_gpus()
ctx =  mx.gpu() if gpus else mx.cpu()

labels = pd.read_excel('./data/label_new.xlsx', columns = ['image', 'label'])
X = np.array(labels['image'])
y = np.array(labels['label'])

X_train_name, X_test_name, y_train, y_test = train_test_split(X, y, test_size = percent)
y_train = mx.nd.array(y_train)
y_test = mx.nd.array(y_test)
def data_storage(name, size, percent, cnt=0):
    matrix = mx.nd.zeros((int(NUM * percent), size, size), ctx=ctx, dtype='uint8')
    for fn in name:
        x = cv.imread(PATH + fn)
        # print(x)
        x = cv.cvtColor(x, cv.COLOR_BGR2GRAY)
        x = cv.resize(x, (size, size))
        matrix[cnt] = x
        cnt += 1
        # break
    matrix = matrix.astype('float32')
    return matrix

train_matrix = data_storage(X_train_name, size, 1 - percent, cnt=0)
test_matrix = data_storage(X_test_name, size, percent, cnt=0)
# train_matrix = mx.nd.reshape(train_matrix, (train_matrix.shape[0], train_matrix.shape[1], train_matrix.shape[2], 1))
# test_matrix = mx.nd.reshape(test_matrix, (test_matrix.shape[0], test_matrix.shape[1], test_matrix.shape[2], 1))

t_data = {"train_matrix": train_matrix, "y_train": y_train, "test_matrix": test_matrix, "y_test": y_test}
filename = "checkpoint.params"
nd.save(filename, t_data)