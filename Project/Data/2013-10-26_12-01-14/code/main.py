import cv2 as cv
import numpy as np
from glob import glob
import xml.etree.ElementTree as ET

# Calibration part

root = ET.parse('data/calibration_data.xml').getroot()  # XML file iterator

camera_metrix = np.reshape(np.fromstring(root[9][3].text, dtype=np.double, sep=' '), (3, 3))
distortion_coefficients = np.fromstring(root[10][3].text, dtype=np.double, sep=' ')

# print(camera_metrix, distortion_coefficients)
newCameraMatrix, poi = cv.getOptimalNewCameraMatrix(camera_metrix,
                                                    distortion_coefficients,
                                                    (2048, 2048), 1, (2048, 2048))

map1, map2 = cv.initUndistortRectifyMap(camera_metrix,
                                        distortion_coefficients,
                                        None,
                                        newCameraMatrix,
                                        (2048, 2048),
                                        cv.CV_16SC2)

z = 7
offset = z * 50

# Image File
PATH = "000006/"
num = 6006
mask = cv.imread("data/mask.bmp")
mask = cv.cvtColor(mask, cv.COLOR_BGR2GRAY)
print(mask.shape)
for fn in glob(PATH + '*.img'):
    img = np.fromfile(fn, dtype= np.uint8)
    img = img.reshape((2048, 2048))
    img = np.transpose(img)
    # cv.imshow("image", img)

    img = np.subtract(img, mask, dtype=int)
    img[img < 0] = 0
    img[img > 255] = 255
    img = img.astype(np.uint8)

    #blur = cv.GaussianBlur(img, (5, 5), 0)

    # contour

    # classification

    # calibration
    img = cv.remap(img, map1, map2, cv.INTER_LINEAR)

    #img = cv.equalizeHist(img)
    temp1 = cv.resize(img, (2048 * z, 2048 * z))
    temp2 = temp1[int(temp1.shape[0] * (0.5 - 1 / z) - offset): int(temp1.shape[0] * (0.5 + 1 / z) - offset),
        int(temp1.shape[1] * (0.5 - 1 / z) - offset): int(temp1.shape[1] * (0.5 + 1 / z) - offset)]

    # img = cv.resize(temp2, (int(temp1.shape[0] * 0.5), int(temp1.shape[0] * 0.5)))
    img = cv.resize(temp2, (2048, 2048))
    img = cv.morphologyEx(img, cv.MORPH_TOPHAT, (101, 101))

    cv.imwrite(str(num) + '.bmp', img)
    print(str(num) + "done")
    num += 1

