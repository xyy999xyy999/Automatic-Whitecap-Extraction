from glob import glob
import cv2 as cv

#PATH = "processed/"
PATH = "./"
name = "thresh_"
num = 6006
for fn in glob(PATH + '*.bmp'):
    img = cv.imread(fn)
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    #cv.namedWindow('image', cv.WINDOW_NORMAL)
    #cv.resizeWindow('image', 1024, 1024)
    # global thresholding
    #ret1, th1 = cv.threshold(img, 141, 255, cv.THRESH_BINARY)
    # Otsu's thresholding
    #ret2, th2 = cv.threshold(img, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
    # Otsu's thresholding after Gaussian filtering
    blur = cv.GaussianBlur(img, (5, 5), 0)
    ret3, th3 = cv.threshold(blur, 192, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
    # Adaptive thresholding
    #th4 = cv.adaptiveThreshold(img, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY_INV, 11, 2)

    cv.imwrite(name + str(num) + ".png", th3)
    num += 1
    #cv.imshow('image', th1)
    #cv.waitKey(0)
    #cv.imshow('image', th2)
    #cv.waitKey(0)
    #cv.imshow('image', th3)
    #cv.waitKey(0)
    #cv.imshow('image', th4)
    #cv.waitKey(0)
    #cv.destroyAllWindows()
