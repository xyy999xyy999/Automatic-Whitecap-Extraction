mask = imreadraw('h:\mask.img');
figure(2)
colormap gray
d = 28;
offset = 0;
%for f=d*1001 + offset:d*1000 + 1000
for f=28708:28708
    fn = sprintf('%06d/%08d.img',d,f)
    subplot(2,2,1)
    im = implot(fn);
    subplot(2,2,2)
    image(im.*mask)
    subplot(2,2,3)
    imhist(im.*mask)
    subplot(2,2,4)
    imagesc(im.*mask > 60)
  %  pause(0.01)
  pause
end




            