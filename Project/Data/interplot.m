thresh = 50;
st = 1;
en = 40000;
figure(1)
plot(chist(st:en,1),chist(st:en,thresh),'b');
pause
while(true)
    figure(1)
    pause(0.01)
    [x,y] = ginput(1);
    x = round(x)
    d = floor(x/1001); 
    ind = find(chist(:,1)==x)
    plot(chist(st:en,1),chist(st:en,thresh),'b');
    hold on
    plot(chist(ind,1),chist(ind,thresh),'r+')
    hold off
    figure(2)
    colormap gray
    subplot(1,3,1)
    fn = sprintf('%06d/%08d.img',d,x)
    im = implot(fn);
    subplot(1,3,2)
    image(im.*mask)
    subplot(1,3,3)
    imagesc(im.*mask > thresh)
end