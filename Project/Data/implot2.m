function im = implot2(filename,mask)
%IMREADRAW Read 8-bit grey level camera image file.
%  Usage: IMAGE = IMREADRAW(FILENAME)
%
%  Parameters: filename - image filename
%              image    - matrix containing image 

%  Author: Steve Gunn (srg@ecs.soton.ac.uk)

    subplot(2,2,1)
    im = implot(filename);
    subplot(2,2,2)
    image(im.*mask)
    subplot(2,2,3)
    imhist(log(1+(im.*mask)))
    subplot(2,2,4)
    imagesc(im.*mask > 60)
    
    